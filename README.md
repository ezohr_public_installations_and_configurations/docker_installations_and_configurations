# Docker Installations And Configurations

## Prerequisites

> None

## Docker Desktop For Mac ([Install Docker Desktop On Mac](https://docs.docker.com/docker-for-mac/install/), [Docker Desktop For Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac/) And [Isolate Containers With A User Namespace](https://docs.docker.com/engine/security/userns-remap/))

### macOS Safari Application (Docker Desktop For Mac)

> Enter: <https://hub.docker.com/editions/community/docker-ce-desktop-mac/>
>
> Select: Get Docker

### macOS Finder Application (Docker Desktop For Mac)

> Double Click: Docker.dmg
>
> Drag And Drop: [Docker To Applications]
>
> Select: Okay

### macOS System Preferences Application (Docker Desktop For Mac Part 1)

> Select: +
>
> Select: Sharing Only
>
> Enter: Docker Remap
>
> Enter: dockremap
>
> Enter: [Password]
>
> Enter: [Password]
>
> Select: Create User
>
> Select: [Image]
>
> Select: [Bird Nest]
>
> Select: Advanced Options...
>
> Enter: 10000

### macOS System Preferences Application (Docker Desktop For Mac Part 2)

> Select: Users And Groups
>
> Select: [Lock]
>
> Enter: [Password]
>
> Select: +
>
> Select: Group
>
> Enter: dockremap
>
> Select: Create Group
>
> Select: Advanced Options...
>
> Enter: 10000
>
> Select: Advanced Options...
>
> Select: dockremap

### Docker Application (Docker Desktop For Mac)

> Select: [Docker In Menu Bar]
>
> Select: Preferences...
>
> Deselect: Send Usage Statistics
>
> Select: Resources
>
> Configure: [Resources]
>
> Select: [Browse Disk Image Location]
>
> Select: [Safe Disk Image Location]
>
> Select: [File Sharing]
>
> Select: [- On All]
>
> Select: +
>
> Select: [Safe Disk Image Location]
>
> Select: Docker Engine
>
> Enter: ([daemon.json](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/DockerInstallationsAndConfigurations/-/blob/master/daemon.json))

```sh
{
  "debug": true,
  "experimental": false,

  # Ideal, but doesn't work in Docker Desktop For Mac
  # "userns-remap": "dockremap"
}
```

### macOS Terminal Application (Docker Desktop For Mac Part 1)

`nano ~/.zshenv`

> Enter: export DOCKER_CONTENT_TRUST=1
>
> Enter: Control + x
>
> Enter: y

### macOS Terminal Application (Docker Desktop For Mac Part 2)

> Enter: sudo visudo
>
> Enter: Defaults env_keep += "DOCKER_CONTENT_TRUST"
>
> Enter: [Escape]
>
> Enter: :wq
